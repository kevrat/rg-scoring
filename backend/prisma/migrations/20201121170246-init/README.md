# Migration `20201121170246-init`

This migration has been generated at 11/21/2020, 5:02:46 PM.
You can check out the [state of the schema](./schema.prisma) after the migration.

## Database Steps

```sql
CREATE TYPE "public"."EnumCategoryType" AS ENUM ('GROUP', 'INDIVIDUAL')

CREATE TYPE "public"."EnumEventState" AS ENUM ('FINISHED', 'OPENED')

CREATE TYPE "public"."EnumJudgePanel" AS ENUM ('PANEL_1', 'PANEL_2')

CREATE TYPE "public"."EnumJudgeType" AS ENUM ('d1', 'd2', 'd3', 'd4', 'e1_2', 'e3', 'e4', 'e5', 'e6')

CREATE TYPE "public"."EnumMissingJudge" AS ENUM ('PANEL_1_E5', 'PANEL_1_E6', 'PANEL_2_E5', 'PANEL_2_E6')

CREATE TYPE "public"."EnumRotationMixType" AS ENUM ('NONE', 'BY_CATEGORIES', 'BY_KINDS')

CREATE TABLE "public"."Category" (
"id" SERIAL,
"createdAt" timestamp(3)   NOT NULL DEFAULT CURRENT_TIMESTAMP,
"updatedAt" timestamp(3)   NOT NULL ,
"name" text   NOT NULL ,
"type" "EnumCategoryType"  NOT NULL ,
"exportLabel" text   ,
PRIMARY KEY ("id")
)

CREATE TABLE "public"."City" (
"id" SERIAL,
"name" text   NOT NULL ,
"createdAt" timestamp(3)   NOT NULL DEFAULT CURRENT_TIMESTAMP,
"updatedAt" timestamp(3)   NOT NULL ,
PRIMARY KEY ("id")
)

CREATE TABLE "public"."Country" (
"id" SERIAL,
"name" text   NOT NULL ,
"createdAt" timestamp(3)   NOT NULL DEFAULT CURRENT_TIMESTAMP,
"updatedAt" timestamp(3)   NOT NULL ,
PRIMARY KEY ("id")
)

CREATE TABLE "public"."Day" (
"id" SERIAL,
"createdAt" timestamp(3)   NOT NULL DEFAULT CURRENT_TIMESTAMP,
"updatedAt" timestamp(3)   NOT NULL ,
"date" timestamp(3)   NOT NULL ,
"eventId" integer   ,
"autopilot" "EnumJudgePanel"[]  ,
"missingJudges" "EnumMissingJudge"[]  ,
PRIMARY KEY ("id")
)

CREATE TABLE "public"."Event" (
"id" SERIAL,
"defaultExportConfigId" integer   NOT NULL ,
"createdAt" timestamp(3)   NOT NULL DEFAULT CURRENT_TIMESTAMP,
"updatedAt" timestamp(3)   NOT NULL ,
"name" text   NOT NULL ,
"state" "EnumEventState"  NOT NULL DEFAULT E'OPENED',
"countryId" integer   ,
"regionId" integer   ,
"cityId" integer   ,
"currentDayId" integer   ,
"isSeparateJudges" boolean   NOT NULL DEFAULT true,
"isStartListByCities" boolean   NOT NULL DEFAULT false,
PRIMARY KEY ("id")
)

CREATE TABLE "public"."ExportConfig" (
"id" SERIAL,
"dateLabel" text   ,
"mainSecretaryLabel" text   ,
"mainJudgeLabel" text   ,
"footer" boolean   NOT NULL ,
"hiddenColumns" text []  ,
PRIMARY KEY ("id")
)

CREATE TABLE "public"."JudgeSeat" (
"id" SERIAL,
"createdAt" timestamp(3)   NOT NULL DEFAULT CURRENT_TIMESTAMP,
"updatedAt" timestamp(3)   NOT NULL ,
"type" "EnumJudgeType"  NOT NULL ,
"panel" "EnumJudgePanel"  NOT NULL ,
"currentPerformanceId" integer   ,
"eventId" integer   NOT NULL ,
PRIMARY KEY ("id")
)

CREATE TABLE "public"."Kind" (
"id" SERIAL,
"createdAt" timestamp(3)   NOT NULL DEFAULT CURRENT_TIMESTAMP,
"updatedAt" timestamp(3)   NOT NULL ,
"name" text   NOT NULL ,
"isApparatus" boolean   NOT NULL ,
PRIMARY KEY ("id")
)

CREATE TABLE "public"."Performance" (
"id" SERIAL,
"createdAt" timestamp(3)   NOT NULL DEFAULT CURRENT_TIMESTAMP,
"updatedAt" timestamp(3)   NOT NULL ,
"mainNumber" integer   NOT NULL ,
"numberInRotation" integer   NOT NULL ,
"judgePanel" "EnumJudgePanel"  NOT NULL DEFAULT E'PANEL_1',
"sportsmanId" integer   ,
"sportsmanGroupId" integer   ,
"kindId" integer   NOT NULL ,
"scoreId" integer   NOT NULL ,
"dayId" integer   NOT NULL ,
"subRotationId" integer   NOT NULL ,
"isShown" boolean   ,
PRIMARY KEY ("id")
)

CREATE TABLE "public"."Region" (
"id" SERIAL,
"name" text   NOT NULL ,
"createdAt" timestamp(3)   NOT NULL DEFAULT CURRENT_TIMESTAMP,
"updatedAt" timestamp(3)   NOT NULL ,
PRIMARY KEY ("id")
)

CREATE TABLE "public"."Rotation" (
"id" SERIAL,
"createdAt" timestamp(3)   NOT NULL DEFAULT CURRENT_TIMESTAMP,
"updatedAt" timestamp(3)   NOT NULL ,
"mainNumber" integer   NOT NULL ,
"name" text   NOT NULL ,
"dayId" integer   NOT NULL ,
"mix" "EnumRotationMixType"  NOT NULL DEFAULT E'NONE',
PRIMARY KEY ("id")
)

CREATE TABLE "public"."SubRotation" (
"id" SERIAL,
"createdAt" timestamp(3)   NOT NULL DEFAULT CURRENT_TIMESTAMP,
"updatedAt" timestamp(3)   NOT NULL ,
"rotationId" integer   NOT NULL ,
PRIMARY KEY ("id")
)

CREATE TABLE "public"."Score" (
"id" SERIAL,
"createdAt" timestamp(3)   NOT NULL DEFAULT CURRENT_TIMESTAMP,
"updatedAt" timestamp(3)   NOT NULL ,
"d1" Decimal(65,30)   ,
"d2" Decimal(65,30)   ,
"d3" Decimal(65,30)   ,
"d4" Decimal(65,30)   ,
"e1_2" Decimal(65,30)   ,
"e3" Decimal(65,30)   ,
"e4" Decimal(65,30)   ,
"e5" Decimal(65,30)   ,
"e6" Decimal(65,30)   ,
"eAverage" Decimal(65,30)   ,
"finalD" Decimal(65,30)   ,
"finalE" Decimal(65,30)   ,
"finalPenalty" Decimal(65,30)   NOT NULL DEFAULT 0,
"finalScore" Decimal(65,30)   ,
PRIMARY KEY ("id")
)

CREATE TABLE "public"."Sportsman" (
"id" SERIAL,
"numberInCategory" integer   NOT NULL ,
"createdAt" timestamp(3)   NOT NULL DEFAULT CURRENT_TIMESTAMP,
"updatedAt" timestamp(3)   NOT NULL ,
"firstName" text   NOT NULL ,
"lastName" text   NOT NULL ,
"trainers" text   NOT NULL ,
"year" text   NOT NULL ,
"rank" text   NOT NULL ,
"club" text   NOT NULL ,
"countryId" integer   ,
"regionId" integer   ,
"cityId" integer   ,
"categoryId" integer   ,
PRIMARY KEY ("id")
)

CREATE TABLE "public"."SportsmanGroup" (
"id" SERIAL,
"numberInCategory" integer   NOT NULL ,
"createdAt" timestamp(3)   NOT NULL DEFAULT CURRENT_TIMESTAMP,
"updatedAt" timestamp(3)   NOT NULL ,
"name" text   NOT NULL ,
"trainers" text   NOT NULL ,
"club" text   NOT NULL ,
"countryId" integer   ,
"regionId" integer   ,
"cityId" integer   ,
"categoryId" integer   NOT NULL ,
PRIMARY KEY ("id")
)

CREATE TABLE "public"."_CategoryToEvent" (
"A" integer   NOT NULL ,
"B" integer   NOT NULL 
)

CREATE TABLE "public"."_categoriesOfFirstJudgePanel" (
"A" integer   NOT NULL ,
"B" integer   NOT NULL 
)

CREATE TABLE "public"."_categoriesOfSecondJudgePanel" (
"A" integer   NOT NULL ,
"B" integer   NOT NULL 
)

CREATE TABLE "public"."_categoryKinds" (
"A" integer   NOT NULL ,
"B" integer   NOT NULL 
)

CREATE TABLE "public"."_categoryKindsBestResults" (
"A" integer   NOT NULL ,
"B" integer   NOT NULL 
)

CREATE TABLE "public"."_EventToExportConfig" (
"A" integer   NOT NULL ,
"B" integer   NOT NULL 
)

CREATE TABLE "public"."_kindsOfFirstJudgePanel" (
"A" integer   NOT NULL ,
"B" integer   NOT NULL 
)

CREATE TABLE "public"."_kindsOfSecondJudgePanel" (
"A" integer   NOT NULL ,
"B" integer   NOT NULL 
)

CREATE TABLE "public"."_SportsmanToSportsmanGroup" (
"A" integer   NOT NULL ,
"B" integer   NOT NULL 
)

CREATE UNIQUE INDEX "City.name_unique" ON "public"."City"("name")

CREATE UNIQUE INDEX "Country.name_unique" ON "public"."Country"("name")

CREATE UNIQUE INDEX "Kind.name_unique" ON "public"."Kind"("name")

CREATE UNIQUE INDEX "Performance.scoreId_unique" ON "public"."Performance"("scoreId")

CREATE UNIQUE INDEX "Region.name_unique" ON "public"."Region"("name")

CREATE UNIQUE INDEX "_CategoryToEvent_AB_unique" ON "public"."_CategoryToEvent"("A", "B")

CREATE INDEX "_CategoryToEvent_B_index" ON "public"."_CategoryToEvent"("B")

CREATE UNIQUE INDEX "_categoriesOfFirstJudgePanel_AB_unique" ON "public"."_categoriesOfFirstJudgePanel"("A", "B")

CREATE INDEX "_categoriesOfFirstJudgePanel_B_index" ON "public"."_categoriesOfFirstJudgePanel"("B")

CREATE UNIQUE INDEX "_categoriesOfSecondJudgePanel_AB_unique" ON "public"."_categoriesOfSecondJudgePanel"("A", "B")

CREATE INDEX "_categoriesOfSecondJudgePanel_B_index" ON "public"."_categoriesOfSecondJudgePanel"("B")

CREATE UNIQUE INDEX "_categoryKinds_AB_unique" ON "public"."_categoryKinds"("A", "B")

CREATE INDEX "_categoryKinds_B_index" ON "public"."_categoryKinds"("B")

CREATE UNIQUE INDEX "_categoryKindsBestResults_AB_unique" ON "public"."_categoryKindsBestResults"("A", "B")

CREATE INDEX "_categoryKindsBestResults_B_index" ON "public"."_categoryKindsBestResults"("B")

CREATE UNIQUE INDEX "_EventToExportConfig_AB_unique" ON "public"."_EventToExportConfig"("A", "B")

CREATE INDEX "_EventToExportConfig_B_index" ON "public"."_EventToExportConfig"("B")

CREATE UNIQUE INDEX "_kindsOfFirstJudgePanel_AB_unique" ON "public"."_kindsOfFirstJudgePanel"("A", "B")

CREATE INDEX "_kindsOfFirstJudgePanel_B_index" ON "public"."_kindsOfFirstJudgePanel"("B")

CREATE UNIQUE INDEX "_kindsOfSecondJudgePanel_AB_unique" ON "public"."_kindsOfSecondJudgePanel"("A", "B")

CREATE INDEX "_kindsOfSecondJudgePanel_B_index" ON "public"."_kindsOfSecondJudgePanel"("B")

CREATE UNIQUE INDEX "_SportsmanToSportsmanGroup_AB_unique" ON "public"."_SportsmanToSportsmanGroup"("A", "B")

CREATE INDEX "_SportsmanToSportsmanGroup_B_index" ON "public"."_SportsmanToSportsmanGroup"("B")

ALTER TABLE "public"."Day" ADD FOREIGN KEY ("eventId")REFERENCES "public"."Event"("id") ON DELETE SET NULL ON UPDATE CASCADE

ALTER TABLE "public"."Event" ADD FOREIGN KEY ("countryId")REFERENCES "public"."Country"("id") ON DELETE SET NULL ON UPDATE CASCADE

ALTER TABLE "public"."Event" ADD FOREIGN KEY ("regionId")REFERENCES "public"."Region"("id") ON DELETE SET NULL ON UPDATE CASCADE

ALTER TABLE "public"."Event" ADD FOREIGN KEY ("cityId")REFERENCES "public"."City"("id") ON DELETE SET NULL ON UPDATE CASCADE

ALTER TABLE "public"."Event" ADD FOREIGN KEY ("currentDayId")REFERENCES "public"."Day"("id") ON DELETE SET NULL ON UPDATE CASCADE

ALTER TABLE "public"."Event" ADD FOREIGN KEY ("defaultExportConfigId")REFERENCES "public"."ExportConfig"("id") ON DELETE CASCADE ON UPDATE CASCADE

ALTER TABLE "public"."JudgeSeat" ADD FOREIGN KEY ("currentPerformanceId")REFERENCES "public"."Performance"("id") ON DELETE SET NULL ON UPDATE CASCADE

ALTER TABLE "public"."JudgeSeat" ADD FOREIGN KEY ("eventId")REFERENCES "public"."Event"("id") ON DELETE CASCADE ON UPDATE CASCADE

ALTER TABLE "public"."Performance" ADD FOREIGN KEY ("dayId")REFERENCES "public"."Day"("id") ON DELETE CASCADE ON UPDATE CASCADE

ALTER TABLE "public"."Performance" ADD FOREIGN KEY ("kindId")REFERENCES "public"."Kind"("id") ON DELETE CASCADE ON UPDATE CASCADE

ALTER TABLE "public"."Performance" ADD FOREIGN KEY ("subRotationId")REFERENCES "public"."SubRotation"("id") ON DELETE CASCADE ON UPDATE CASCADE

ALTER TABLE "public"."Performance" ADD FOREIGN KEY ("scoreId")REFERENCES "public"."Score"("id") ON DELETE CASCADE ON UPDATE CASCADE

ALTER TABLE "public"."Performance" ADD FOREIGN KEY ("sportsmanGroupId")REFERENCES "public"."SportsmanGroup"("id") ON DELETE SET NULL ON UPDATE CASCADE

ALTER TABLE "public"."Performance" ADD FOREIGN KEY ("sportsmanId")REFERENCES "public"."Sportsman"("id") ON DELETE SET NULL ON UPDATE CASCADE

ALTER TABLE "public"."Rotation" ADD FOREIGN KEY ("dayId")REFERENCES "public"."Day"("id") ON DELETE CASCADE ON UPDATE CASCADE

ALTER TABLE "public"."SubRotation" ADD FOREIGN KEY ("rotationId")REFERENCES "public"."Rotation"("id") ON DELETE CASCADE ON UPDATE CASCADE

ALTER TABLE "public"."Sportsman" ADD FOREIGN KEY ("cityId")REFERENCES "public"."City"("id") ON DELETE SET NULL ON UPDATE CASCADE

ALTER TABLE "public"."Sportsman" ADD FOREIGN KEY ("countryId")REFERENCES "public"."Country"("id") ON DELETE SET NULL ON UPDATE CASCADE

ALTER TABLE "public"."Sportsman" ADD FOREIGN KEY ("regionId")REFERENCES "public"."Region"("id") ON DELETE SET NULL ON UPDATE CASCADE

ALTER TABLE "public"."Sportsman" ADD FOREIGN KEY ("categoryId")REFERENCES "public"."Category"("id") ON DELETE SET NULL ON UPDATE CASCADE

ALTER TABLE "public"."SportsmanGroup" ADD FOREIGN KEY ("cityId")REFERENCES "public"."City"("id") ON DELETE SET NULL ON UPDATE CASCADE

ALTER TABLE "public"."SportsmanGroup" ADD FOREIGN KEY ("countryId")REFERENCES "public"."Country"("id") ON DELETE SET NULL ON UPDATE CASCADE

ALTER TABLE "public"."SportsmanGroup" ADD FOREIGN KEY ("regionId")REFERENCES "public"."Region"("id") ON DELETE SET NULL ON UPDATE CASCADE

ALTER TABLE "public"."SportsmanGroup" ADD FOREIGN KEY ("categoryId")REFERENCES "public"."Category"("id") ON DELETE CASCADE ON UPDATE CASCADE

ALTER TABLE "public"."_CategoryToEvent" ADD FOREIGN KEY ("A")REFERENCES "public"."Category"("id") ON DELETE CASCADE ON UPDATE CASCADE

ALTER TABLE "public"."_CategoryToEvent" ADD FOREIGN KEY ("B")REFERENCES "public"."Event"("id") ON DELETE CASCADE ON UPDATE CASCADE

ALTER TABLE "public"."_categoriesOfFirstJudgePanel" ADD FOREIGN KEY ("A")REFERENCES "public"."Category"("id") ON DELETE CASCADE ON UPDATE CASCADE

ALTER TABLE "public"."_categoriesOfFirstJudgePanel" ADD FOREIGN KEY ("B")REFERENCES "public"."Rotation"("id") ON DELETE CASCADE ON UPDATE CASCADE

ALTER TABLE "public"."_categoriesOfSecondJudgePanel" ADD FOREIGN KEY ("A")REFERENCES "public"."Category"("id") ON DELETE CASCADE ON UPDATE CASCADE

ALTER TABLE "public"."_categoriesOfSecondJudgePanel" ADD FOREIGN KEY ("B")REFERENCES "public"."Rotation"("id") ON DELETE CASCADE ON UPDATE CASCADE

ALTER TABLE "public"."_categoryKinds" ADD FOREIGN KEY ("A")REFERENCES "public"."Category"("id") ON DELETE CASCADE ON UPDATE CASCADE

ALTER TABLE "public"."_categoryKinds" ADD FOREIGN KEY ("B")REFERENCES "public"."Kind"("id") ON DELETE CASCADE ON UPDATE CASCADE

ALTER TABLE "public"."_categoryKindsBestResults" ADD FOREIGN KEY ("A")REFERENCES "public"."Category"("id") ON DELETE CASCADE ON UPDATE CASCADE

ALTER TABLE "public"."_categoryKindsBestResults" ADD FOREIGN KEY ("B")REFERENCES "public"."Kind"("id") ON DELETE CASCADE ON UPDATE CASCADE

ALTER TABLE "public"."_EventToExportConfig" ADD FOREIGN KEY ("A")REFERENCES "public"."Event"("id") ON DELETE CASCADE ON UPDATE CASCADE

ALTER TABLE "public"."_EventToExportConfig" ADD FOREIGN KEY ("B")REFERENCES "public"."ExportConfig"("id") ON DELETE CASCADE ON UPDATE CASCADE

ALTER TABLE "public"."_kindsOfFirstJudgePanel" ADD FOREIGN KEY ("A")REFERENCES "public"."Kind"("id") ON DELETE CASCADE ON UPDATE CASCADE

ALTER TABLE "public"."_kindsOfFirstJudgePanel" ADD FOREIGN KEY ("B")REFERENCES "public"."Rotation"("id") ON DELETE CASCADE ON UPDATE CASCADE

ALTER TABLE "public"."_kindsOfSecondJudgePanel" ADD FOREIGN KEY ("A")REFERENCES "public"."Kind"("id") ON DELETE CASCADE ON UPDATE CASCADE

ALTER TABLE "public"."_kindsOfSecondJudgePanel" ADD FOREIGN KEY ("B")REFERENCES "public"."Rotation"("id") ON DELETE CASCADE ON UPDATE CASCADE

ALTER TABLE "public"."_SportsmanToSportsmanGroup" ADD FOREIGN KEY ("A")REFERENCES "public"."Sportsman"("id") ON DELETE CASCADE ON UPDATE CASCADE

ALTER TABLE "public"."_SportsmanToSportsmanGroup" ADD FOREIGN KEY ("B")REFERENCES "public"."SportsmanGroup"("id") ON DELETE CASCADE ON UPDATE CASCADE
```

## Changes

```diff
diff --git schema.prisma schema.prisma
migration ..20201121170246-init
--- datamodel.dml
+++ datamodel.dml
@@ -1,0 +1,284 @@
+generator client {
+  provider        = "prisma-client-js"
+  previewFeatures = ["transactionApi"]
+}
+
+datasource db {
+  provider = "postgresql"
+  url = "***"
+}
+
+model Category {
+  id                             Int              @id @default(autoincrement())
+  createdAt                      DateTime         @default(now())
+  updatedAt                      DateTime         @updatedAt
+  name                           String
+  type                           EnumCategoryType
+  events                         Event[]
+  sportsmen                      Sportsman[]
+  sportsmanGroups                SportsmanGroup[]
+  exportLabel                    String?
+  rotationsWhereFirstJudgePanel  Rotation[]       @relation("categoriesOfFirstJudgePanel")
+  rotationsWhereSecondJudgePanel Rotation[]       @relation("categoriesOfSecondJudgePanel")
+  kinds                          Kind[]           @relation("categoryKinds")
+  kindsBestResults               Kind[]           @relation("categoryKindsBestResults")
+}
+
+model City {
+  id              Int              @id @default(autoincrement())
+  name            String           @unique
+  createdAt       DateTime         @default(now())
+  updatedAt       DateTime         @updatedAt
+  events          Event[]
+  sportsmen       Sportsman[]
+  sportsmanGroups SportsmanGroup[]
+}
+
+model Country {
+  id              Int              @id @default(autoincrement())
+  name            String           @unique
+  createdAt       DateTime         @default(now())
+  updatedAt       DateTime         @updatedAt
+  events          Event[]
+  sportsmen       Sportsman[]
+  sportsmanGroups SportsmanGroup[]
+}
+
+model Day {
+  id                    Int                @id @default(autoincrement())
+  createdAt             DateTime           @default(now())
+  updatedAt             DateTime           @updatedAt
+  date                  DateTime
+  eventId               Int?
+  event                 Event?             @relation(fields: [eventId], references: [id])
+  eventsWhereCurrentDay Event[]            @relation("eventsWhereCurrentDay")
+  performances          Performance[]
+  rotations             Rotation[]
+  autopilot             EnumJudgePanel[]
+  missingJudges         EnumMissingJudge[]
+}
+
+model Event {
+  id                    Int            @id @default(autoincrement())
+  defaultExportConfigId Int
+  createdAt             DateTime       @default(now())
+  updatedAt             DateTime       @updatedAt
+  name                  String
+  state                 EnumEventState @default(OPENED)
+  country               Country?       @relation(fields: [countryId], references: [id])
+  region                Region?        @relation(fields: [regionId], references: [id])
+  city                  City?          @relation(fields: [cityId], references: [id])
+  countryId             Int?
+  regionId              Int?
+  cityId                Int?
+  currentDayId          Int?
+  currentDay            Day?           @relation("eventsWhereCurrentDay", fields: [currentDayId], references: [id])
+  days                  Day[]
+  judgeSeats            JudgeSeat[]
+  categories            Category[]
+  defaultExportConfig   ExportConfig   @relation("eventsWhereDefault", fields: [defaultExportConfigId], references: [id])
+  exportConfigs         ExportConfig[]
+  isSeparateJudges      Boolean        @default(true)
+  isStartListByCities   Boolean        @default(false)
+}
+
+model ExportConfig {
+  id                 Int      @id @default(autoincrement())
+  events             Event[]
+  eventsWhereDefault Event[]  @relation("eventsWhereDefault")
+  dateLabel          String?
+  mainSecretaryLabel String?
+  mainJudgeLabel     String?
+  footer             Boolean
+  hiddenColumns      String[]
+}
+
+model JudgeSeat {
+  id                   Int            @id @default(autoincrement())
+  createdAt            DateTime       @default(now())
+  updatedAt            DateTime       @updatedAt
+  type                 EnumJudgeType
+  panel                EnumJudgePanel
+  currentPerformanceId Int?
+  eventId              Int
+  currentPerformance   Performance?   @relation(fields: [currentPerformanceId], references: [id])
+  event                Event          @relation(fields: [eventId], references: [id])
+}
+
+model Kind {
+  id                             Int           @id @default(autoincrement())
+  createdAt                      DateTime      @default(now())
+  updatedAt                      DateTime      @updatedAt
+  name                           String        @unique
+  isApparatus                    Boolean
+  performances                   Performance[]
+  rotationsWhereFirstJudgePanel  Rotation[]    @relation("kindsOfFirstJudgePanel")
+  rotationsWhereSecondJudgePanel Rotation[]    @relation("kindsOfSecondJudgePanel")
+  categories                     Category[]    @relation("categoryKinds")
+  categoriesWhereBestResults     Category[]    @relation("categoryKindsBestResults")
+}
+
+model Performance {
+  id                                Int             @id @default(autoincrement())
+  createdAt                         DateTime        @default(now())
+  updatedAt                         DateTime        @updatedAt
+  mainNumber                        Int
+  numberInRotation                  Int
+  judgePanel                        EnumJudgePanel  @default(PANEL_1)
+  sportsmanId                       Int?
+  sportsmanGroupId                  Int?
+  kindId                            Int
+  scoreId                           Int             @unique
+  dayId                             Int
+  // rotationId                        Int
+  subRotationId                     Int
+  day                               Day             @relation(fields: [dayId], references: [id])
+  kind                              Kind            @relation(fields: [kindId], references: [id])
+  // rotation                          Rotation        @relation(fields: [rotationId], references: [id])
+  subRotation                       SubRotation     @relation(fields: [subRotationId], references: [id])
+  score                             Score           @relation(fields: [scoreId], references: [id])
+  sportsmanGroup                    SportsmanGroup? @relation(fields: [sportsmanGroupId], references: [id])
+  sportsman                         Sportsman?      @relation(fields: [sportsmanId], references: [id])
+  judgeSeatsWhereCurrentPerformance JudgeSeat[]
+  isShown                           Boolean?
+}
+
+model Region {
+  id              Int              @id @default(autoincrement())
+  name            String           @unique
+  createdAt       DateTime         @default(now())
+  updatedAt       DateTime         @updatedAt
+  events          Event[]
+  sportsmen       Sportsman[]
+  sportsmanGroups SportsmanGroup[]
+}
+
+model Rotation {
+  id                           Int                 @id @default(autoincrement())
+  createdAt                    DateTime            @default(now())
+  updatedAt                    DateTime            @updatedAt
+  mainNumber                   Int
+  name                         String
+  dayId                        Int
+  day                          Day                 @relation(fields: [dayId], references: [id])
+  // performances Performance[]
+  categoriesOfFirstJudgePanel  Category[]          @relation("categoriesOfFirstJudgePanel")
+  categoriesOfSecondJudgePanel Category[]          @relation("categoriesOfSecondJudgePanel")
+  kindsOfFirstJudgePanel       Kind[]              @relation("kindsOfFirstJudgePanel")
+  kindsOfSecondJudgePanel      Kind[]              @relation("kindsOfSecondJudgePanel")
+  mix                          EnumRotationMixType @default(NONE)
+  subRotations                 SubRotation[]
+}
+
+model SubRotation {
+  id           Int           @id @default(autoincrement())
+  createdAt    DateTime      @default(now())
+  updatedAt    DateTime      @updatedAt
+  rotationId   Int
+  rotation     Rotation      @relation(fields: [rotationId], references: [id])
+  performances Performance[]
+}
+
+model Score {
+  id           Int          @id @default(autoincrement())
+  createdAt    DateTime     @default(now())
+  updatedAt    DateTime     @updatedAt
+  d1           Float?
+  d2           Float?
+  d3           Float?
+  d4           Float?
+  e1_2         Float?
+  e3           Float?
+  e4           Float?
+  e5           Float?
+  e6           Float?
+  eAverage     Float?
+  finalD       Float?
+  finalE       Float?
+  finalPenalty Float        @default(0)
+  finalScore   Float?
+  performance  Performance?
+}
+
+model Sportsman {
+  id               Int              @id @default(autoincrement())
+  numberInCategory Int
+  createdAt        DateTime         @default(now())
+  updatedAt        DateTime         @updatedAt
+  firstName        String
+  lastName         String
+  trainers         String
+  year             String
+  rank             String
+  club             String
+  countryId        Int?
+  regionId         Int?
+  cityId           Int?
+  categoryId       Int?
+  city             City?            @relation(fields: [cityId], references: [id])
+  country          Country?         @relation(fields: [countryId], references: [id])
+  region           Region?          @relation(fields: [regionId], references: [id])
+  performances     Performance[]
+  category         Category?        @relation(fields: [categoryId], references: [id])
+  sportsmanGroups  SportsmanGroup[]
+}
+
+model SportsmanGroup {
+  id               Int           @id @default(autoincrement())
+  numberInCategory Int
+  createdAt        DateTime      @default(now())
+  updatedAt        DateTime      @updatedAt
+  name             String
+  trainers         String
+  club             String
+  countryId        Int?
+  regionId         Int?
+  cityId           Int?
+  categoryId       Int
+  city             City?         @relation(fields: [cityId], references: [id])
+  country          Country?      @relation(fields: [countryId], references: [id])
+  region           Region?       @relation(fields: [regionId], references: [id])
+  performances     Performance[]
+  category         Category      @relation(fields: [categoryId], references: [id])
+  sportsmen        Sportsman[]
+}
+
+enum EnumCategoryType {
+  GROUP
+  INDIVIDUAL
+}
+
+enum EnumEventState {
+  FINISHED
+  OPENED
+}
+
+enum EnumJudgePanel {
+  PANEL_1
+  PANEL_2
+}
+
+enum EnumJudgeType {
+  d1
+  d2
+  d3
+  d4
+  e1_2
+  e3
+  e4
+  e5
+  e6
+}
+
+enum EnumMissingJudge {
+  PANEL_1_E5
+  PANEL_1_E6
+  PANEL_2_E5
+  PANEL_2_E6
+}
+
+enum EnumRotationMixType {
+  NONE
+  BY_CATEGORIES
+  BY_KINDS
+}
```


