import { PrismaClient } from '@prisma/client'
import geo from './geo'
// import event from './event'
import kinds from './kinds'

async function seed () {
    const prisma = new PrismaClient()
    await geo()
    // await event()
    await kinds()

    await prisma.$disconnect()
}

seed()