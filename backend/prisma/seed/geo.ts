import { PrismaClient } from '@prisma/client'

export default async function seed () {
    const prisma = new PrismaClient()
    const countries = ["Россия"]
    const regions = [
        "Рязанская область",
        "Ивановская область",
        "Московская область",
        "Москва",
        "Ярославская область",
        "Краснодарский край",
        "Ленинградская область",
        "Санкт-Петербург",
    ]
    const cities = [
        "Рязань",
        "Иваново",
        "Ступино",
        "Москва",
        "Внуково",
        "Ярославль",
        "Сочи",
        "Сланцы",
        "Санкт-Петербург",
        "Сланцы",
    ]
    await prisma.$transaction(countries.map(name =>
        prisma.country.upsert({
            where: { name },
            update: { name },
            create: { name },
        })))
    await prisma.$transaction(regions.map(name =>
        prisma.region.upsert({
            where: { name },
            update: { name },
            create: { name },
        })))
    await prisma.$transaction(cities.map(name =>
        prisma.city.upsert({
            where: { name },
            update: { name },
            create: { name },
        })))

    await prisma.$disconnect()
}
