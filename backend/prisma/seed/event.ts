import { PrismaClient } from '@prisma/client'

export default async function seed () {
    const prisma = new PrismaClient()


    await prisma.event.create({
        data: {
            name: 'Есенинская осень',
            state: "OPENED",
            country: {
                connect: {
                    name: "Россия"
                }
            },
            region: {
                connect: {
                    name: "Рязанская область"
                }
            },
            city: {
                connect: {
                    name: "Рязань"
                }
            },
            days: {
                create: [
                    {
                        date: new Date("2020-09-18")
                    },
                    {
                        date: new Date("2020-09-19")
                    },
                    {
                        date: new Date("2020-09-20")
                    }
                ]
            },
            defaultExportConfig: {
                create: {
                    dateLabel: '16-20 сентября 2019 года',
                    mainSecretaryLabel: '__________________________Главный секретарь Иванова И. А.',
                    mainJudgeLabel: '__________________________Главный судья Иванова И. А.',
                    footer: false,
                    hiddenColumns: {
                        set: ['rank']
                    }
                }
            },
            categories: {
                create: [
                    // INDIVIDUAL
                    {
                        name: "2007 г.р.",
                        type: "INDIVIDUAL",
                    },
                    {
                        name: "2008 г.р.",
                        type: "INDIVIDUAL",
                    },
                    // {
                    //     name: "2009 г.р.",
                    //     type: "INDIVIDUAL",
                    // },
                    // {
                    //     name: "2010 г.р.",
                    //     type: "INDIVIDUAL",
                    // },
                    // {
                    //     name: "2011 и старше",
                    //     type: "INDIVIDUAL",
                    // },
                    // {
                    //     name: "МС",
                    //     type: "INDIVIDUAL",
                    // },
                    // {
                    //     name: "КМС",
                    //     type: "INDIVIDUAL",
                    // },
                    // {
                    //     name: "3р.",
                    //     type: "INDIVIDUAL",
                    // },
                    // {
                    //     name: "2р.",
                    //     type: "INDIVIDUAL",
                    // },
                    // {
                    //     name: "1р.",
                    //     type: "INDIVIDUAL",
                    // },
                    // GROUP
                    {
                        name: "МС",
                        type: "GROUP",
                    },
                    {
                        name: "КМС",
                        type: "GROUP",
                    },
                    // {
                    //     name: "3р.",
                    //     type: "GROUP",
                    // },
                    // {
                    //     name: "2р.",
                    //     type: "GROUP",
                    // },
                    // {
                    //     name: "2008",
                    //     type: "GROUP",
                    // },
                    // {
                    //     name: "2007",
                    //     type: "GROUP",
                    // }
                ]
            },
            judgeSeats: {
                create: [
                    // PANEL_1
                    {
                        type: "e1_2",
                        panel: "PANEL_1"
                    },
                    {
                        type: "e3",
                        panel: "PANEL_1"
                    },
                    {
                        type: "e4",
                        panel: "PANEL_1"
                    },
                    {
                        type: "e5",
                        panel: "PANEL_1"
                    },
                    {
                        type: "e6",
                        panel: "PANEL_1"
                    },
                    {
                        type: "d1",
                        panel: "PANEL_1"
                    },
                    {
                        type: "d2",
                        panel: "PANEL_1"
                    },
                    {
                        type: "d3",
                        panel: "PANEL_1"
                    },
                    {
                        type: "d4",
                        panel: "PANEL_1"
                    },
                    // PANEL_2
                    {
                        type: "e1_2",
                        panel: "PANEL_2"
                    },
                    {
                        type: "e3",
                        panel: "PANEL_2"
                    },
                    {
                        type: "e4",
                        panel: "PANEL_2"
                    },
                    {
                        type: "e5",
                        panel: "PANEL_2"
                    },
                    {
                        type: "e6",
                        panel: "PANEL_2"
                    },
                    {
                        type: "d1",
                        panel: "PANEL_2"
                    },
                    {
                        type: "d2",
                        panel: "PANEL_2"
                    },
                    {
                        type: "d3",
                        panel: "PANEL_2"
                    },
                    {
                        type: "d4",
                        panel: "PANEL_2"
                    },
                ]
            }
        },
    })

    await prisma.$disconnect()
}
