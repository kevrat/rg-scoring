import { objectType } from '@nexus/schema'

export default objectType({
    name: 'JudgeConnection',
    definition (t) {
        t.string('uuid')
        t.int('judgeSeatId')
    },
})