import { objectType } from '@nexus/schema'

export default objectType({
    name: 'Event',
    definition (t) {
        t.model.id()
        t.model.name()
        t.model.createdAt()
        t.model.updatedAt()
        t.model.state()
        t.model.country()
        t.model.region()
        t.model.city()
        t.model.categories()
        t.model.days({
            ordering: true,
        })
        t.model.currentDay()
        t.model.judgeSeats()
        t.model.defaultExportConfig()
        t.model.exportConfigs()
        t.model.isSeparateJudges()
        t.model.isStartListByCities()
    },
})