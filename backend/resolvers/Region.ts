import { objectType } from '@nexus/schema'

export default objectType({
    name: 'Region',
    definition (t) {
        t.model.id()
        t.model.createdAt()
        t.model.updatedAt()
        t.model.name()
        t.model.events()
        t.model.sportsmen()
        t.model.sportsmanGroups()
    },
})