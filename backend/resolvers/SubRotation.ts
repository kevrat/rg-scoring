import { objectType } from '@nexus/schema'

export default objectType({
    name: 'SubRotation',
    definition (t) {
        t.model.id()
        t.model.createdAt()
        t.model.updatedAt()
        t.model.performances({
            ordering: true,
        })
        t.model.rotation()
    },
})