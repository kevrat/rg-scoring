import { extendType } from '@nexus/schema'

export default extendType({
    type: 'Mutation',
    definition (t) {
        t.crud.upsertOneEvent()
        
        t.crud.createOneEvent()
        t.crud.deleteOneEvent()

        t.crud.updateOneCategory()
        t.crud.upsertOneCategory()
        t.crud.deleteOneCategory()

        t.crud.upsertOneRegion()

        t.crud.upsertOneCountry()

        t.crud.upsertOneCity()

        t.crud.createOneRotation()
        t.crud.updateOneRotation()
        t.crud.deleteOneRotation()
        t.crud.deleteManyRotation()

        t.crud.updateOneJudgeSeat()
        t.crud.createOneJudgeSeat()


        t.crud.updateOnePerformance()
        t.crud.deleteManyPerformance()

        t.crud.updateOneSportsman()
        t.crud.updateOneSportsmanGroup()
    },
})
