import { extendType, idArg, arg, intArg, floatArg, stringArg } from '@nexus/schema'
import { EnumJudgeType, EnumJudgePanel, Score, EnumMissingJudge } from '@prisma/client'
import { Context } from '../../context'

export default extendType({
    type: 'Mutation',
    definition (t) {

        t.field('prevPerformanceForJudgeSeat', {
            type: 'JudgeSeat',
            args: {
                dayId: intArg({ required: true }),
                type: arg({ type: 'EnumJudgeType', required: true }),
                panel: arg({ type: 'EnumJudgePanel', required: true })
            },
            nullable: true,
            resolve: async (_, { dayId, type, panel }: { dayId: number, type: EnumJudgeType, panel: EnumJudgePanel }, ctx) => {
                const judgeSeat = await getJudgeSeat(dayId, type, panel, ctx)
                if (!judgeSeat) {
                    return null
                }
                const isJudgeSeatD2D4 = judgeSeat.type === 'd2' || judgeSeat.type === 'd4'
                const currentJudgeSeatPerformance = await ctx.prisma.judgeSeat.findOne({ where: { id: judgeSeat.id } }).currentPerformance()

                const nextPerformance = await ctx.prisma.performance.findFirst({
                    where: {
                        day: {
                            id: dayId
                        },
                        mainNumber: {
                            lt: currentJudgeSeatPerformance ? currentJudgeSeatPerformance.mainNumber : 2
                        },
                        judgePanel: judgeSeat.panel,
                        kind: isJudgeSeatD2D4 ? {
                            isApparatus: false
                        } : undefined,
                        NOT: isJudgeSeatD2D4 ? [{
                            sportsman: null
                        }] : undefined

                    },
                    take: 1,
                    orderBy: {
                        mainNumber: 'desc'
                    }
                })
                if (!nextPerformance) {
                    return null
                }
                await ctx.prisma.judgeSeat.update({
                    where: { id: judgeSeat.id },
                    data: {
                        currentPerformance: {
                            connect: {
                                id: nextPerformance.id
                            }
                        }
                    },
                })

                const judgeSeatUpdated = await ctx.prisma.judgeSeat.findOne({
                    where: {
                        id: judgeSeat.id
                    },
                    include: {
                        event: {
                            include: {
                                days: {
                                    select: {
                                        id: true
                                    }
                                },

                            }
                        }
                    }
                })
                ctx.pubsub.publish("JUDGE_PANEL_UPDATED", {
                    judgePanelUpdated: [judgeSeatUpdated]
                })
                ctx.pubsub.publish("JUDGE_SEATS_UPDATED", {
                    judgeSeatsUpdated: [judgeSeatUpdated]
                })
                return judgeSeatUpdated
            }
        });
        t.field('prevPerformanceForAllJudgeSeat', {
            type: 'JudgeSeat',
            args: {
                dayId: intArg({ required: true }),
                panel: arg({ type: 'EnumJudgePanel', required: true })
            },
            nullable: true,
            list: true,
            resolve: async (_, { dayId, panel }: { dayId: number, panel: EnumJudgePanel }, ctx) => {
                const judgeSeats = await getJudgeSeats(dayId, ['d1', 'd2', 'd3', 'd4', 'e1_2', 'e3', 'e4', 'e5', 'e6'], panel, ctx)
                if (judgeSeats.length === 0) {
                    return null
                }

                const currentJudgeSeatPerformance = await ctx.prisma.judgeSeat.findFirst({
                    where: {
                        id: {
                            in: judgeSeats.map(judgeSeat => judgeSeat.id)
                        }
                    }
                }).currentPerformance()

                const nextPerformance = await ctx.prisma.performance.findFirst({
                    where: {
                        day: {
                            id: dayId
                        },
                        mainNumber: {
                            lt: currentJudgeSeatPerformance ? currentJudgeSeatPerformance.mainNumber : 2
                        },
                        judgePanel: panel,
                        score: {
                            OR: [
                                { finalScore: { equals: 0 } },
                                { finalScore: null, }
                            ]
                        },

                    },
                    orderBy: {
                        mainNumber: 'desc'
                    }
                })
                if (!nextPerformance) {
                    return null
                }
                const judgeSeatsUpdated = await ctx.prisma.$transaction(
                    judgeSeats.map((judgeSeat) =>
                        ctx.prisma.judgeSeat.update({
                            where: { id: judgeSeat.id },
                            data: {
                                currentPerformance: {
                                    connect: {
                                        id: nextPerformance.id
                                    }
                                }
                            },
                            include: {
                                event: {
                                    include: {
                                        days: {
                                            select: {
                                                id: true
                                            }
                                        },

                                    }
                                }
                            }
                        })
                    )
                )
                ctx.pubsub.publish("JUDGE_PANEL_UPDATED", {
                    judgePanelUpdated: judgeSeatsUpdated
                })
                ctx.pubsub.publish("JUDGE_SEATS_UPDATED", {
                    judgeSeatsUpdated
                })
                return judgeSeatsUpdated
            }
        });

        t.field('setPerformanceForJudgeSeats', {
            type: 'JudgeSeat',
            args: {
                dayId: intArg({ required: true }),
                judgeTypes: arg({ type: 'EnumJudgeType', required: true, list: true }),
                panel: arg({ type: 'EnumJudgePanel', required: true }),
                mainNumber: intArg({ required: true })
            },
            nullable: true,
            list: true,
            resolve: async (_, { dayId, judgeTypes, panel, mainNumber }: { dayId: number, judgeTypes: [EnumJudgeType], panel: EnumJudgePanel, mainNumber: number }, ctx) => {
                const judgeSeats = await getJudgeSeats(dayId, judgeTypes, panel, ctx)
                if (judgeSeats.length === 0) {
                    return null
                }
                const toSetPerformance = await ctx.prisma.performance.findFirst({
                    where: {
                        day: {
                            id: dayId
                        },
                        mainNumber,
                        judgePanel: {
                            in: judgeSeats.map(judgeSeat => judgeSeat.panel)
                        },

                    },
                    orderBy: {
                        mainNumber: 'asc'
                    }
                })
                if (!toSetPerformance) {
                    return null
                }
                const judgeSeatsUpdated = await ctx.prisma.$transaction(
                    judgeSeats.map((judgeSeat) =>
                        ctx.prisma.judgeSeat.update({
                            where: { id: judgeSeat.id },
                            data: {
                                currentPerformance: {
                                    connect: {
                                        id: toSetPerformance.id
                                    }
                                }
                            },

                            include: {
                                event: {
                                    include: {
                                        days: {
                                            select: {
                                                id: true
                                            }
                                        },

                                    }
                                }
                            }
                        })
                    )
                )
                ctx.pubsub.publish("JUDGE_PANEL_UPDATED", {
                    judgePanelUpdated: judgeSeatsUpdated
                })
                ctx.pubsub.publish("JUDGE_SEATS_UPDATED", {
                    judgeSeatsUpdated
                })
                return judgeSeatsUpdated
            }
        });
        t.field('nextPerformanceForJudgeSeat', {
            type: 'JudgeSeat',
            args: {
                dayId: intArg({ required: true }),
                type: arg({ type: 'EnumJudgeType', required: true }),
                panel: arg({ type: 'EnumJudgePanel', required: true })
            },
            nullable: true,
            resolve: async (_, { dayId, type, panel }: { dayId: number, type: EnumJudgeType, panel: EnumJudgePanel }, ctx) => {
                return nextPerformanceForJudgeSeat(dayId, type, panel, ctx)
            }
        });
        t.field('nextPerformanceForAllJudgeSeat', {
            type: 'JudgeSeat',
            args: {
                dayId: intArg({ required: true }),
                panel: arg({ type: 'EnumJudgePanel', required: true })
            },
            nullable: true,
            list: true,
            resolve: async (_, { dayId, panel }: { dayId: number, panel: EnumJudgePanel }, ctx) => {
                return nextPerformanceForJudgeSeats(dayId, ['d1', 'd2', 'd3', 'd4', 'e1_2', 'e3', 'e4', 'e5', 'e6'], panel, ctx)
            }
        });
        t.field('ratePerformance', {
            type: 'Performance',
            args: {
                id: intArg({ required: true }),
                type: stringArg({ required: true }),
                point: floatArg({ required: true })
            },
            nullable: true,
            resolve: async (_, { id, type, point }: { id: number, type: string, point: number }, ctx) => {
                const performance = await ctx.prisma.performance.findOne({ where: { id }, include: { score: true, kind: true, sportsman: true, day: { include: { event: true } } } })
                const score = performance?.score
                const isApparatus = performance?.kind.isApparatus
                const isIndividualWithoutApparatus = isApparatus ? false : !!(performance?.sportsman)
                if (!score || !['e1_2', 'e3', 'e4', 'e5', 'e6', 'd1', 'd2', 'd3', 'd4', 'finalPenalty'].includes(type)) {
                    return null
                }
                const missingJudgesE = performance.day.missingJudges.filter(missingJudge => missingJudge.includes(performance.judgePanel))
                const scoreTemp: Score = { ...score, [type]: point }
                console.log([scoreTemp.e3, scoreTemp.e4, scoreTemp.e5, scoreTemp.e6].slice(0, (4 - missingJudgesE.length)))
                const eAverage = getEAverage([scoreTemp.e3, scoreTemp.e4, scoreTemp.e5, scoreTemp.e6].slice(0, (4 - missingJudgesE.length)))
                const finalE = getFinalE({ ...scoreTemp, eAverage })
                const finalD = isIndividualWithoutApparatus ? getFinalDForIndividualWithoutApparatus(scoreTemp) : getFinalDCommon(scoreTemp)
                const finalScore = getFinalScore({ finalE, finalD, finalPenalty: scoreTemp.finalPenalty })

                await ctx.prisma.score.update({
                    where: { id: score.id },
                    data: {
                        [type]: point,
                        eAverage: eAverage !== score.eAverage ? eAverage : undefined,
                        finalE: finalE !== score.finalE ? finalE : undefined,
                        finalD: finalD !== score.finalD ? finalD : undefined,
                        finalScore: finalScore !== score.finalScore ? finalScore : undefined,
                    },
                })
                if (['d1', 'd2', 'd3', 'd4', 'e1_2', 'e3', 'e4', 'e5', 'e6'].includes(type)) {
                    if (performance?.day.autopilot.includes(performance.judgePanel)) {
                        if (performance.day.event?.isSeparateJudges) {
                            const judgeSeat = await ctx.prisma.judgeSeat.findFirst({
                                where: {
                                    currentPerformance: {
                                        id
                                    },
                                    type: type as EnumJudgeType
                                },
                                include: {
                                    currentPerformance: {
                                        include: {
                                            day: true
                                        }
                                    }
                                }
                            })
                            if (judgeSeat?.currentPerformance?.day.id) {
                                await nextPerformanceForJudgeSeat(judgeSeat.currentPerformance.day.id, judgeSeat.type, judgeSeat.panel, ctx)
                            }

                        } else {
                            if (finalScore > 0) {
                                const judgeSeats = await ctx.prisma.judgeSeat.findMany({
                                    where: {
                                        currentPerformance: {
                                            id
                                        }
                                    },
                                    include: {
                                        currentPerformance: {
                                            include: {
                                                day: true
                                            }
                                        }
                                    }
                                })
                                if (judgeSeats.length > 0) {
                                    const dayId = judgeSeats[0].currentPerformance.day.id
                                    const judgeTypes = judgeSeats.map(judgeSeat => judgeSeat.type)
                                    const panel = judgeSeats[0].panel
                                    await nextPerformanceForJudgeSeats(dayId, judgeTypes, panel, ctx)
                                }

                            }
                        }

                    }

                }

                const scoreUpdated = await ctx.prisma.score.findFirst({
                    where: {
                        performance: {
                            id
                        }
                    },
                    include: {
                        performance: {
                            include: {
                                day: true
                            }
                        }
                    }
                })
                if (scoreUpdated) {
                    ctx.pubsub.publish("SCORE_UPDATED", {
                        scoreUpdated
                    })
                }
                return ctx.prisma.performance.findOne({ where: { id } })
            }
        });
        t.field('showPerformance', {
            type: 'Performance',
            args: {
                id: intArg({ required: true }),
            },
            nullable: true,
            resolve: async (_, { id }: { id: number }, ctx) => {
                const performanceShowed = await ctx.prisma.performance.update({
                    where: { id },
                    data: { isShown: true },
                    include: {
                        day: {
                            include: {
                                event: {
                                    select: {
                                        id: true
                                    }
                                }
                            }
                        }
                    }
                })

                ctx.pubsub.publish("PERFORMANCE_SHOWED", {
                    performanceShowed
                })
                return performanceShowed
            }
        });
        t.field('toggleMissingJudge', {
            type: 'Day',
            args: {
                dayId: intArg({ required: true }),
                missingJudge: arg({ type: 'EnumMissingJudge', required: true }),
            },
            nullable: true,
            resolve: async (_, { dayId, missingJudge }: { dayId: number, missingJudge: EnumMissingJudge }, ctx) => {
                const day = await ctx.prisma.day.findOne({
                    where: { id: dayId }
                })
                if (!day) {
                    return null
                }
                const dayWhereMissingJudgeToggled = await ctx.prisma.day.update({
                    where: { id: dayId },
                    data: {
                        missingJudges: {
                            set: day.missingJudges.includes(missingJudge)
                                ? day.missingJudges.filter(missingJudge1 => missingJudge1 !== missingJudge)
                                : [...day.missingJudges, missingJudge]
                        }
                    },
                    include: {
                        event: {
                            include: {
                                days: true
                            }
                        }
                    }
                })
                ctx.pubsub.publish("EVENT_UPDATED", {
                    eventUpdated: dayWhereMissingJudgeToggled.event
                })

                return dayWhereMissingJudgeToggled
            }
        });
        t.field('clearScore', {
            type: 'Performance',
            args: {
                mainNumberToClear: intArg({ required: true }),
                dayId: intArg({ required: true }),
            },
            nullable: true,
            resolve: async (_, { mainNumberToClear, dayId }: { mainNumberToClear: number, dayId: number }, ctx) => {
                const performanceToUpdate = await ctx.prisma.performance.findFirst({
                    where: {
                        mainNumber: mainNumberToClear,
                        day: {
                            id: dayId
                        }
                    }
                })
                if (!performanceToUpdate) {
                    return null
                }
                const updatedPerformance = await ctx.prisma.performance.update({
                    where: { id: performanceToUpdate.id },
                    data: {
                        score: {
                            update: {
                                d1: null,
                                d2: null,
                                d3: null,
                                d4: null,
                                e1_2: null,
                                e3: null,
                                e4: null,
                                e5: null,
                                e6: null,
                                eAverage: null,
                                finalD: null,
                                finalE: null,
                                finalPenalty: 0,
                                finalScore: null
                            }
                        },
                        isShown: false
                    },
                    include: {
                        score: {
                            include: {
                                performance: {
                                    include: {
                                        day: true
                                    }
                                }
                            }
                        }
                    }
                })

                if (updatedPerformance) {
                    ctx.pubsub.publish("SCORE_UPDATED", {
                        scoreUpdated: updatedPerformance.score
                    })
                }
                return updatedPerformance
            }
        });
    },
})


const nextPerformanceForJudgeSeat = async (dayId: number, type: EnumJudgeType, panel: EnumJudgePanel, ctx: Context) => {
    const judgeSeat = await getJudgeSeat(dayId, type, panel, ctx)
    if (judgeSeat === null) {
        return null
    }
    const isJudgeSeatD2D4 = judgeSeat.type === 'd2' || judgeSeat.type === 'd4'
    const currentJudgeSeatPerformance = await ctx.prisma.judgeSeat.findOne({ where: { id: judgeSeat.id } }).currentPerformance()
    const nextPerformance = (await ctx.prisma.performance.findFirst({
        where: {
            day: {
                id: dayId
            },
            mainNumber: {
                gt: currentJudgeSeatPerformance ? currentJudgeSeatPerformance.mainNumber : 0
            },
            judgePanel: judgeSeat.panel,
            kind: isJudgeSeatD2D4 ? {
                isApparatus: false
            } : undefined,
            NOT: isJudgeSeatD2D4 ? [{
                sportsman: null
            }] : undefined

        },
        orderBy: {
            mainNumber: 'asc'
        }
    }))
    if (nextPerformance) {
        await ctx.prisma.judgeSeat.update({
            where: { id: judgeSeat.id },
            data: {
                currentPerformance: {
                    connect: {
                        id: nextPerformance.id
                    }
                }
            },
        })
    }
    const judgeSeatUpdated = await ctx.prisma.judgeSeat.findOne({
        where: {
            id: judgeSeat.id
        },
        include: {
            event: {
                include: {
                    days: {
                        select: {
                            id: true
                        }
                    },

                }
            }
        }
    })
    ctx.pubsub.publish("JUDGE_PANEL_UPDATED", {
        judgePanelUpdated: [judgeSeatUpdated]
    })
    ctx.pubsub.publish("JUDGE_SEATS_UPDATED", {
        judgeSeatsUpdated: [judgeSeatUpdated]
    })
    return judgeSeatUpdated

}
const nextPerformanceForJudgeSeats = async (dayId: number, judgeTypes: [EnumJudgeType], panel: EnumJudgePanel, ctx: Context) => {
    const judgeSeats = await getJudgeSeats(dayId, judgeTypes, panel, ctx)
    if (judgeSeats.length === 0) {
        return null
    }
    const currentJudgeSeatPerformance = await ctx.prisma.judgeSeat.findFirst({
        where: {
            id: {
                in: judgeSeats.map(judgeSeat => judgeSeat.id)
            }
        }
    }).currentPerformance()

    if (!currentJudgeSeatPerformance) {
        return null
    }
    const nextPerformance = await ctx.prisma.performance.findFirst({
        where: {
            day: {
                id: dayId
            },
            mainNumber: {
                gt: currentJudgeSeatPerformance?.mainNumber ?? 0
            },
            judgePanel: panel,
            score: {
                OR: [
                    { finalScore: { equals: 0 } },
                    { finalScore: null, }
                ]
            },

        },
        orderBy: {
            mainNumber: 'asc'
        }
    })
    if (!nextPerformance) {
        return null
    }

    const judgeSeatsUpdated = await ctx.prisma.$transaction(
        judgeSeats.map((judgeSeat) =>
            ctx.prisma.judgeSeat.update({
                where: { id: judgeSeat.id },
                data: {
                    currentPerformance: {
                        connect: {
                            id: nextPerformance.id
                        }
                    }
                },
                include: {
                    event: {
                        include: {
                            days: {
                                select: {
                                    id: true
                                }
                            },

                        }
                    }
                }
            })
        )
    )

    ctx.pubsub.publish("JUDGE_PANEL_UPDATED", {
        judgePanelUpdated: judgeSeatsUpdated
    })
    ctx.pubsub.publish("JUDGE_SEATS_UPDATED", {
        judgeSeatsUpdated
    })
    return judgeSeatsUpdated

}
const getJudgeSeat = async (dayId: number, type: EnumJudgeType, panel: EnumJudgePanel, ctx: Context) => {
    const judgeSeat = await ctx.prisma.judgeSeat.findFirst({
        where: {
            event: {
                days: {
                    some: {
                        id: dayId
                    }
                }
            },
            type,
            panel
        },
    })
    return judgeSeat

}
const getJudgeSeats = async (dayId: number, judgeTypes: [EnumJudgeType], panel: EnumJudgePanel, ctx: Context) => {
    const judgeSeats = await ctx.prisma.judgeSeat.findMany({
        where: {
            event: {
                days: {
                    some: {
                        id: dayId
                    }
                }
            },
            type: {
                in: judgeTypes
            },
            panel
        },
    })
    return judgeSeats

}

const round = (num: number) => {
    // return num
    return +num.toFixed(2)
    // return Math.round(num / 100) * 100
}

const getFinalScore = ({ finalE, finalD, finalPenalty }: { finalE: number | null, finalD: number | null, finalPenalty: number }) => {
    if (finalE === null || finalD === null || finalPenalty === null) return null
    return round(finalE + finalD - finalPenalty)
}

const getEAverage = (points: any[]) => {
    if (points.some(point => (point === null || point === 0))) {
        return null
    }
    if (points.length === 4) {
        return round(getAverageOfTwoMiddlePoints(points))
    }
    if (points.length === 3) {
        return round(points.sort()[1])
    }
    if (points.length === 2) {
        return round((points[0] + points[1]) / 2)
    }
    return null
}
const getFinalDForIndividualWithoutApparatus = ({ d1 = null, d2 = null, d3 = null, d4 = null, }: { d1?: number | null, d2?: number | null, d3?: number | null, d4?: number | null }) => {
    if (d1 === null || d2 === null || d3 === null || d4 === null) return null
    return round(getAverageOfTwoMiddlePoints([d1, d2, d3, d4]))
}
const getFinalDCommon = ({ d1 = null, d3 = null }: { d1?: number | null, d3?: number | null }) => {
    if (d1 === null || d3 === null) return null
    return round(d1 + d3)
}
const getFinalE = ({ e1_2 = null, eAverage = null }: { e1_2?: number | null, eAverage?: number | null }) => {
    if (e1_2 === null || eAverage === null) return null
    const finalE = round(10 - e1_2 - eAverage)
    return finalE >= 0 ? finalE : 0
}
const getAverageOfTwoMiddlePoints = (points: any[]) => {
    return round(points.sort().slice(1, 3).reduce((sum, p) => p + sum, 0) / 2)
}