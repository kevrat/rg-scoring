import PrismaMutations from './prisma'
import Scoring from './scoring'
import StartLists from './startLists'
import Events from './events'
import Categories from './categories'

export default [PrismaMutations, Scoring, StartLists, Events, Categories]