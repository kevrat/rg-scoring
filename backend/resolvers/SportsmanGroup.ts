import { objectType } from '@nexus/schema'

export default objectType({
    name: 'SportsmanGroup',
    definition (t) {
        t.model.id()
        t.model.numberInCategory()
        t.model.createdAt()
        t.model.updatedAt()
        t.model.name()
        t.model.trainers()
        t.model.club()
        t.model.country()
        t.model.region()
        t.model.city()
        t.model.sportsmen({
            ordering: true,
        })
        t.model.category()
        t.model.performances()
    },
})