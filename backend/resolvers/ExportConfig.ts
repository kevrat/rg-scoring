import { objectType } from '@nexus/schema'

export default objectType({
    name: 'ExportConfig',
    definition (t) {
        t.model.id()
        t.model.dateLabel()
        t.model.mainSecretaryLabel()
        t.model.mainJudgeLabel()
        t.model.footer()
        t.model.hiddenColumns()
        t.model.events()
        t.model.eventsWhereDefault()
    },
})