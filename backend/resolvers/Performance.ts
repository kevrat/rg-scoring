import { objectType } from '@nexus/schema'

export default objectType({
    name: 'Performance',
    definition (t) {
        t.model.id()
        t.model.createdAt()
        t.model.updatedAt()
        t.model.mainNumber()
        t.model.numberInRotation()
        t.model.judgePanel()
        t.model.sportsman()
        t.model.sportsmanGroup()
        t.model.kind()
        t.model.score()
        t.model.day()
        t.model.subRotation()
        t.model.isShown()
    },
})