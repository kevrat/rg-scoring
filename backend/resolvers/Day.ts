import { objectType } from '@nexus/schema'

export default objectType({
    name: 'Day',
    definition (t) {
        t.model.id()
        t.model.createdAt()
        t.model.updatedAt()
        t.model.date()
        t.model.eventsWhereCurrentDay()
        t.model.event()
        t.model.performances({ pagination: true })
        t.model.autopilot()
        t.model.missingJudges()
    },
})