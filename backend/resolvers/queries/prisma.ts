import { extendType } from '@nexus/schema'
import { arg } from '@nexus/schema'
import { connectionFromArray } from 'graphql-relay'

export default extendType({
    type: 'Query',
    definition (t) {
        t.connectionField('performancesConnection', {
            type: 'Performance',
            additionalArgs: {
                where: arg({
                    type: 'PerformanceWhereInput',
                    nullable: true,
                }),
                orderBy: arg({
                    type: 'PerformanceOrderByInput',
                    nullable: true,
                })
            },
            async resolve (root, args, ctx, info) {
                return connectionFromArray(await ctx.prisma.performance.findMany({
                    where: args.where,
                    orderBy: args.orderBy
                }), args)

            },



        })
        t.crud.categories({
            ordering: true,
            filtering: true,
        })
        t.crud.category()
        t.crud.cities({
            ordering: true,
            filtering: true,
        })
        t.crud.countries({
            ordering: true,
            filtering: true,
        })
        t.crud.days({
            ordering: true,
            filtering: true,
        })
        t.crud.events({
            ordering: true,
            filtering: true,
        })
        t.crud.judgeSeats({
            ordering: true,
            filtering: true,
        })
        t.crud.kinds({
            ordering: true,
            filtering: true,
        })
        t.crud.performances({
            ordering: true,
            pagination: true,
            filtering: true,
        })
        t.crud.regions({
            ordering: true,
            filtering: true,
        })
        t.crud.rotations({
            ordering: true,
            filtering: true,
        })
        t.crud.scores({
            ordering: true,
            filtering: true,
        })
        t.crud.sportsmen({
            ordering: true,
            filtering: true,
        })
        t.crud.sportsmanGroups({
            ordering: true,
            filtering: true,
        })
    },
})
