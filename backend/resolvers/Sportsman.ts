import { objectType } from '@nexus/schema'

export default objectType({
    name: 'Sportsman',
    definition (t) {
        t.model.id()
        t.model.numberInCategory()
        t.model.createdAt()
        t.model.updatedAt()
        t.model.firstName()
        t.model.lastName()
        t.model.trainers()
        t.model.year()
        t.model.rank()
        t.model.club()
        t.model.country()
        t.model.region()
        t.model.city()
        t.model.sportsmanGroups()
        t.model.category()
        t.model.performances()
    },
})