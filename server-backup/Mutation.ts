import { stringArg, idArg, floatArg, mutationType, arg, intArg } from 'nexus'
import { EnumJudgeType, EnumJudgePanel } from '../generated/prisma-client'
import { Context } from '../types';

export const Mutation = mutationType({
  definition (t) {

    t.field('ratePerformance', {
      type: 'Performance',
      args: { id: idArg({ required: true }), type: stringArg({ required: true }), point: floatArg({ nullable: true }) },
      nullable: true,
      resolve: async (_, { id, type, point }: { id: string, type: string, point?: number | null | undefined }, ctx) => {
        const score = await ctx.prisma.performance({ id }).score()
        const { isApparatus } = await ctx.prisma.performance({ id }).kind()
        const isIndividualWithoutApparatus = isApparatus ? false : !!(await ctx.prisma.performance({ id }).sportsman())
        if (!score || !['e1_2', 'e3', 'e4', 'e5', 'e6', 'd1', 'd2', 'd3', 'd4', 'finalPenalty'].includes(type)) {
          return null
        }
        const scoreTemp = { ...score, [type as string]: point }
        const eAverage = getEAverage(scoreTemp)
        const finalE = getFinalE({ ...scoreTemp, eAverage })
        const finalD = isIndividualWithoutApparatus ? getFinalDForIndividualWithoutApparatus(scoreTemp) : getFinalDCommon(scoreTemp)
        const finalScore = getFinalScore({ finalE, finalD, finalPenalty: scoreTemp.finalPenalty })

        await ctx.prisma.updateScore({
          where: { id: score.id },
          data: {
            [type as string]: point,
            eAverage: eAverage !== score.eAverage ? eAverage : undefined,
            finalE: finalE !== score.finalE ? finalE : undefined,
            finalD: finalD !== score.finalD ? finalD : undefined,
            finalScore: finalScore !== score.finalScore ? finalScore : undefined,
          },
        })
        const judgeType: any = type.toUpperCase()
        if (['D1', 'D2', 'D3', 'D4', 'E1_2', 'E3', 'E4', 'E5', 'E6'].includes(judgeType)) {
          const judgeSeats: any = await ctx.prisma.judgeSeats({
            where: {
              currentPerformance: {
                id
              },
              type: judgeType
            },
            first: 1
          }).$fragment(`
          fragment JudgeSeatWithDay on JudgeSeat {
            currentPerformance{
              day{
                id
              }
            }
            type
            panel
          }
          `)
          if (judgeSeats.length !== 0) {
            const judgeSeat = judgeSeats[0]
            await nextPerformanceForJudgeSeat(judgeSeat.currentPerformance.day.id, judgeSeat.type, judgeSeat.panel, ctx)
          }

        }
        return ctx.prisma.performance({ id })
      }
    });
    t.field('prevPerformanceForJudgeSeat', {
      type: 'JudgeSeat',
      args: { dayId: idArg({ required: true }), type: arg({ type: 'EnumJudgeType', required: true }), panel: arg({ type: 'EnumJudgePanel', required: true }) },//JudgeSeatWhereInput
      nullable: true,
      resolve: async (_, { dayId, type, panel }: { dayId: string, type: EnumJudgeType, panel: EnumJudgePanel }, ctx) => {
        const judgeSeat = await getJudgeSeat(dayId, type, panel, ctx)
        if (judgeSeat === null) {
          return null
        }
        const isJudgeSeatD2D4 = judgeSeat.type === 'D2' || judgeSeat.type === 'D4'
        const currentJudgeSeatPerformance = await ctx.prisma.judgeSeat({ id: judgeSeat.id }).currentPerformance()
        const nextPerformances = (await ctx.prisma.performances({
          where: {
            day: {
              id: dayId
            },
            mainNumber_lt: currentJudgeSeatPerformance ? currentJudgeSeatPerformance.mainNumber : 2,
            judgePanel: judgeSeat.panel,
            kind: isJudgeSeatD2D4 ? {
              isApparatus: false
            } : undefined,
            sportsman: isJudgeSeatD2D4 ? {
              id_not: null
            } : undefined,

          },
          first: 1,
          orderBy: {
            mainNumber: 'desc'
          }
        }))
        if (nextPerformances.length !== 0) {
          await ctx.prisma.updateJudgeSeat({
            where: { id: judgeSeat.id },
            data: {
              currentPerformance: {
                connect: {
                  id: nextPerformances[0].id
                }
              }
            },
          })
        }
        return ctx.prisma.judgeSeat({ id: judgeSeat.id })
      }
    });
    t.field('nextPerformanceForJudgeSeat', {
      type: 'JudgeSeat',
      args: { dayId: idArg({ required: true }), type: arg({ type: 'EnumJudgeType', required: true }), panel: arg({ type: 'EnumJudgePanel', required: true }) },//JudgeSeatWhereInput
      nullable: true,
      resolve: async (_, { dayId, type, panel }: { dayId: string, type: EnumJudgeType, panel: EnumJudgePanel }, ctx) => {
        return nextPerformanceForJudgeSeat(dayId, type, panel, ctx)
      }
    });
    t.field('setPerformanceForJudgeSeat', {
      type: 'JudgeSeat',
      args: { dayId: idArg({ required: true }), type: arg({ type: 'EnumJudgeType', required: true }), panel: arg({ type: 'EnumJudgePanel', required: true }), mainNumber: intArg({ required: true }) },//JudgeSeatWhereInput
      nullable: true,
      resolve: async (_, { dayId, type, panel, mainNumber }: { dayId: string, type: EnumJudgeType, panel: EnumJudgePanel, mainNumber: number }, ctx) => {
        const judgeSeat = await getJudgeSeat(dayId, type, panel, ctx)
        if (judgeSeat === null) {
          return null
        }
        const toSetPerformances = (await ctx.prisma.performances({
          where: {
            day: {
              id: dayId
            },
            mainNumber,
            judgePanel: judgeSeat.panel,

          },
          first: 1,
          orderBy: {
            mainNumber: 'asc'
          }
        }))
        if (toSetPerformances.length !== 0) {
          await ctx.prisma.updateJudgeSeat({
            where: { id: judgeSeat.id },
            data: {
              currentPerformance: {
                connect: {
                  id: toSetPerformances[0].id
                }
              }
            },
          })
        }
        return ctx.prisma.judgeSeat({ id: judgeSeat.id })
      }
    });
  },
})

const getFinalScore = ({ finalE, finalD, finalPenalty }: { finalE: number | null, finalD: number | null, finalPenalty: number }) => {
  if (finalE === null || finalD === null || finalPenalty === null) return null
  return finalE + finalD - finalPenalty
}

const getEAverage = ({ e3, e4, e5, e6 }: { e3?: number, e4?: number, e5?: number, e6?: number }) => {
  if (e3 === null || e4 === null || e5 === null || e6 === null) return null
  return getAverageOfTwoMiddlePoints([e3, e4, e5, e6])
}
const getFinalDForIndividualWithoutApparatus = ({ d1 = null, d2 = null, d3 = null, d4 = null, }: { d1?: number | null, d2?: number | null, d3?: number | null, d4?: number | null }) => {
  if (d1 === null || d2 === null || d3 === null || d4 === null) return null
  return getAverageOfTwoMiddlePoints([d1, d2, d3, d4])
}
const getFinalDCommon = ({ d1 = null, d3 = null }: { d1?: number | null, d3?: number | null }) => {
  if (d1 === null || d3 === null) return null
  return d1 + d3
}
const getFinalE = ({ e1_2 = null, eAverage = null }: { e1_2?: number | null, eAverage?: number | null }) => {
  if (e1_2 === null || eAverage === null) return null
  return 10 - e1_2 - eAverage
}
const getAverageOfTwoMiddlePoints = (points: any[]) => {
  return points.sort().slice(1, 3).reduce((sum, p) => p + sum, 0) / 2
}

const nextPerformanceForJudgeSeat = async (dayId: string, type: EnumJudgeType, panel: EnumJudgePanel, ctx: Context) => {
  const judgeSeat = await getJudgeSeat(dayId, type, panel, ctx)
  if (judgeSeat === null) {
    return null
  }
  const isJudgeSeatD2D4 = judgeSeat.type === 'D2' || judgeSeat.type === 'D4'
  const currentJudgeSeatPerformance = await ctx.prisma.judgeSeat({ id: judgeSeat.id }).currentPerformance()
  const nextPerformances = (await ctx.prisma.performances({
    where: {
      day: {
        id: dayId
      },
      mainNumber_gt: currentJudgeSeatPerformance ? currentJudgeSeatPerformance.mainNumber : 0,
      judgePanel: judgeSeat.panel,
      kind: isJudgeSeatD2D4 ? {
        isApparatus: false
      } : undefined,
      sportsman: isJudgeSeatD2D4 ? {
        id_not: null
      } : undefined,

    },
    first: 1,
    orderBy: {
      mainNumber: 'asc'
    }
  }))
  if (nextPerformances.length !== 0) {
    await ctx.prisma.updateJudgeSeat({
      where: { id: judgeSeat.id },
      data: {
        currentPerformance: {
          connect: {
            id: nextPerformances[0].id
          }
        }
      },
    })
  }
  return ctx.prisma.judgeSeat({ id: judgeSeat.id })

}

const getJudgeSeat = async (dayId: string, type: EnumJudgeType, panel: EnumJudgePanel, ctx: Context) => {
  const judgeSeats = await ctx.prisma.judgeSeats({
    where: {
      event: {
        days: {
          some: {
            id: { equals: dayId }
          }
        }
      },
      type,
      panel
    },
    first: 1
  })
  const day = await ctx.prisma.day({ id: dayId })
  if (judgeSeats.length === 0 || day === null) {
    return null
  }
  return judgeSeats[0]

}