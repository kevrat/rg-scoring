FROM node:12-alpine as build
RUN apk add --no-cache --virtual .build-deps \
    python \
    make \
    g++
WORKDIR /usr/src/app
COPY package.json yarn.lock ./
RUN yarn
RUN yarn global add @quasar/cli
# RUN apk del .build-deps
# ENV PATH /usr/src/app/node_modules/.bin:$PATH
COPY . ./
RUN quasar build