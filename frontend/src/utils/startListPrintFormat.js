
import { extend } from 'quasar'
const rowsForPrint = (rotation, allCategories) => {
  const subRotations = extend(true, {}, rotation).subRotations
  // const maxSubRotationSize = Math.max(...subRotations.map(subRotation => subRotation.performances.length))
  const maxRowsCount = subRotations.reduce((maxRowsCount, subRotation) => maxRowsCount + subRotation.performances.length, 0)
  const rows = makeRowsForPrint(maxRowsCount, subRotations.map(subRotation => ({
    performances: [...subRotation.performances.slice()]
  })), allCategories)
  return rows
  // const rowsReverse = makeRowsForPrint(maxRowsCount, subRotations.map(subRotation => ({
  //   performances: [...subRotation.performances.slice().reverse()]
  // })), true).reverse()
  // const countLeftEmptyPerformances = (rows) =>
  //   rows.reduce((emptyLeftPerformancesCount, row) => row.performances[0] ? emptyLeftPerformancesCount : emptyLeftPerformancesCount++, 0)

  // return countLeftEmptyPerformances(rows) < countLeftEmptyPerformances(rowsReverse) ? rows : rowsReverse
  // return rows.length < rowsReverse.length ? rows : rowsReverse
}

const getIndexOfClosestParticipant = (participantId, performances) =>
  performances.findIndex(performance => {
    if (!performance) {
      return
    }
    return participantId === (performance.sportsman ?? performance.sportsmanGroup).id
  })
const makeRowsForPrint = (maxRowsCount, subRotations, allCategories) => {
  return [...Array(maxRowsCount)].map((_, i) => {
    const subRotationsCurrent = subRotations
    const subRotationIndex = subRotationsCurrent.findIndex(subRotation => subRotation.performances[0])
    const performancesCurrent = subRotationsCurrent[subRotationIndex]?.performances
    const performanceCurrent = performancesCurrent?.[0]
    if (!performanceCurrent) {
      return
    }
    let participantCurrent = performanceCurrent.sportsman ?? performanceCurrent.sportsmanGroup
    if (participantCurrent && subRotationIndex < subRotationsCurrent.length - 1) {
      const subRotationsCurrentOpposite = subRotationsCurrent.slice().reverse()
      const subRotationOppositeIndex = subRotationsCurrentOpposite.findIndex(subRotation => subRotation.performances[0])
      const performancesOpposite = subRotationsCurrentOpposite[subRotationOppositeIndex].performances
      const performanceOpposite = performancesOpposite[0]
      const participantOpposite = performanceOpposite.sportsman ?? performanceOpposite.sportsmanGroup
      if (participantOpposite && participantCurrent.id !== participantOpposite.id) {
        const index1 = getIndexOfClosestParticipant(participantCurrent.id, performancesOpposite)
        const index2 = getIndexOfClosestParticipant(participantOpposite.id, performancesCurrent)
        if ((index2 === -1 && performancesCurrent.length > 1) || (index1 > -1 && index1 < index2) || (i === 0 && index1 === 1)) {
          participantCurrent = participantOpposite
        }
      }
    }
    const performances = subRotations.map(subRotation => {
      const performance = subRotation.performances[0]
      if (performance) {
        const participant = performance.sportsman ?? performance.sportsmanGroup
        if ((!participantCurrent && !participant) || (participant?.id === participantCurrent?.id)) {
          return subRotation.performances.splice(0, 1)[0]
        }
      }
    })
    return {
      participant: participantCurrent,
      availableKinds: allCategories.find(category => category.id === participantCurrent?.category?.id)?.kinds ?? [],
      performances
    }
  })
    .filter(row => row)
}

export {
  rowsForPrint
}
