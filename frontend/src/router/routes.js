
const routes = [
  {
    path: '/judge',
    component: () => import('layouts/Tablet.vue'),
    children: [
      { path: '', component: () => import('pages/Judge.vue') }
    ]
  },
  {
    path: '/admin',
    component: () => import('layouts/Admin.vue'),
    children: [
      { path: '', component: () => import('pages/Index.vue') },
      {
        name: 'events', path: 'events', component: () => import('pages/Events.vue')
      }
    ]
  },
  {
    path: '/screen',
    component: () => import('layouts/Screen.vue'),
    children: [
      { path: '', component: () => import('pages/Screen.vue') },
      {
        name: 'screen', path: 'screen', component: () => import('pages/Screen.vue')
      }
    ]
  },
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '', component: () => import('pages/Index.vue') },
      {
        name: 'categories', path: 'categories', component: () => import('pages/Categories.vue')
      },
      { name: 'start-lists', path: 'start-lists', component: () => import('pages/StartLists.vue') },
      {
        name: 'scoring', path: 'scoring', component: () => import('pages/Scoring.vue')
      },
      {
        name: 'results', path: 'results', component: () => import('pages/Results.vue')
      },
      {
        name: 'participants', path: 'participants', component: () => import('pages/Participants.vue')
      }
    ]
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '*',
    component: () => import('pages/Error404.vue')
  }
]

export default routes
