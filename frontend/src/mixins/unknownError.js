const unknownErrorMixin = {
  methods: {
    unknownError (error) {
      this.$q.notify({
        color: 'red',
        message: error.message || error
      })
      console.error(error)
    }
  }
}
export default unknownErrorMixin
