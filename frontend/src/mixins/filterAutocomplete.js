const filterAutocompleteMixin = {
  methods: {
    async filterAutocomplete (val, update, abort, query) {
      if (val.length < 2) {
        abort()
        return
      }
      // prevent empty variables error by skiping query
      query.setOptions({
        variables: this.getVariables(val),
        fetchPolicy: 'network-only'
      })
      await query.start()
      update()
    }
  }
}
export default filterAutocompleteMixin
