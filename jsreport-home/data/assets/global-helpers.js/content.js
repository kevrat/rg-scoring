/* click debug to see internal js structure of sheet1, it makes it easier for you
   to write the path pattern in xlsxAdd, xlsxMerge...
   
   the best is to copy the json into a visualiser like http://json.parser.online.fr/
*/
const handlebars = require('handlebars');

const helpers = require('handlebars-helpers')({
    handlebars: handlebars
});
Handlebars.unregisterHelper("year");

function log(file, opts) {
    console.log(JSON.stringify(opts.data.root.$xlsxTemplate[file]))
}

function consoleLog(value) {
    console.log(value)
}

function getLetterFromAlphabetByIndex(length) {
    alphabet = "abcdefghijklmnopqrstuvwxyz"
    return alphabet[length]
}

function getSecretaryMergeRange(signRowNumber, hiddenColumns) {
    if (hiddenColumns.includes('region')) {
        return mergeCellRange("A" + signRowNumber, 0, 5, signRowNumber)
    }
    return mergeCellRange("A" + signRowNumber, 0, 4, signRowNumber)
}

function getJudgeMergeRange(signRowNumber, hiddenColumns, kindsLength) {
    if (hiddenColumns.includes('region')) {
        return mergeCellRange("G" + signRowNumber, 8, kindsLength, signRowNumber)
    }
    return mergeCellRange("F" + signRowNumber, 8, kindsLength, signRowNumber)
}

function mergeCellRange(start, shift, length, columnNumber) {
    return start + ':' + getLetterFromAlphabetByIndex(shift + length) + columnNumber
}

function concatenate(lvalue, rvalue) {
    return lvalue + rvalue
}

function math(lvalue, operator, rvalue, options) {
    if (arguments.length < 4) {
        // Operator omitted, assuming "+"
        options = rvalue;
        rvalue = operator;
        operator = "+";
    }

    lvalue = parseFloat(lvalue);
    rvalue = parseFloat(rvalue);
    return {
        "+": lvalue + rvalue,
        "-": lvalue - rvalue,
        "*": lvalue * rvalue,
        "/": lvalue / rvalue,
        "%": lvalue % rvalue
    }[operator];
}

function setVar(varName, varValue, options) {
    console.log(varName, varValue)
    options.data.root[varName] = varValue;
}

function x(expression, options) {
    var result;

    // you can change the context, or merge it with options.data, options.hash
    // var context = Object.assign(this, options.data.root);
    var context = options.data.root;

    // yup, i use 'with' here to expose the context's properties as block variables
    // you don't need to do {{x 'this.age + 2'}}
    // but you can also do {{x 'age + 2'}}
    // HOWEVER including an UNINITIALIZED var in a expression will return undefined as the result.
    with(context) {
        result = (function() {
            try {
                return eval(expression);
            } catch (e) {
                console.warn('•Expression: {{x \'' + expression + '\'}}\n•JS-Error: ', e, '\n•Context: ', context);
            }
        }).call(context); // to make eval's lexical this=context
    }
    return result;

}

function xif(expression, options) {
    return handlebars.helpers["x"].apply(this, [expression, options]) ? options.fn(this) : options.inverse(this);
}

function min(a, b, options) {
    return a < b ? a : b
}

function max(a, b, options) {
    return a > b ? a : b
}

function ceil(a, options) {
    return Math.ceil(a)
}

function formatParticipant(participant) {
    if (!participant) {
        return '-'
    }
    const {
        lastName,
        firstName,
        name
    } = participant
    return name || `${lastName || ''} ${firstName || ''}`
}

function times(n, block) {
    var accum = '';
    for (var i = 0; i < n; ++i) {
        block.data.index = i;
        block.data.first = i === 0;
        block.data.last = i === (n - 1);
        accum += block.fn(this);
    }
    return accum;
}


function getMergeCellRangeForRotationAt(startCol, startRow, rotations, rotationIndex) {
    const rowIndex = startRow + 1 + rotations.slice(0, rotationIndex).reduce((rowsCount, rotation) => {
        return rowsCount + 2 + rotation.rows.length
    }, 0)
    const rows = rotations[rotationIndex].rows
    const performancesColsCount = rows.length > 0 ? rows[0].performances.length : rotations.reduce((maxPerformancesColsCount, rotation) => {
        return Math.max(maxPerformancesColsCount, rotation.rows.length > 0 ? rotation.rows[0].performances.length : 0)
    }, 0)
    return startCol + rowIndex + ':' + getLetterFromAlphabetByIndex(6 + performancesColsCount) + rowIndex

}